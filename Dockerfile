FROM python:3.6
ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE settings.local
RUN mkdir /code
WORKDIR /code
COPY . /code/
#RUN sudo apt-get update
#RUN sudo apt-get -y install python-pip
#RUN sudo apt-get update
#RUN pip install --upgrade pip
#RUN pip -y install psycopg2-binary
RUN pip install -r requirements.txt
COPY . /code/
RUN python manage.py makemigrations && python manage.py migrate
EXPOSE 8000
#CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
