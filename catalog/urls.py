from django.urls import path, include

from .views import CoursesView, CoursesFilterView

urlpatterns = [
    path('', CoursesView.as_view(), name='all'),
    path('<filter_category>/', CoursesFilterView.as_view(), name='detail'),
]