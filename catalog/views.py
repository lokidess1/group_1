from django.shortcuts import render
from django.views.generic import TemplateView
from django.db.models import Count

from course.models import Course, Category
from django.contrib.auth.models import User


class CoursesView(TemplateView):
    template_name = 'catalog/courses.html'

    def get_context_data(self, **kwargs):
        context = super(CoursesView, self).get_context_data(**kwargs)
        context["courses"] = Course.objects.select_related('category').annotate(count=Count('purchased_by')).order_by('-count')
        context["categorys"] = Category.objects.all()

        return context


class CoursesFilterView(TemplateView):
    template_name = 'catalog/courses.html'

    def get(self, request, filter_category):
        try:
            courses = Course.objects.filter(
                category__name=filter_category
            ).select_related('category').annotate(count=Count('purchased_by')).order_by('-count')
            
        except IndexError:
            courses = None

        categorys = Category.objects.all()

        return render(request, self.template_name, {'courses': courses, 'categorys': categorys})