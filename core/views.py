from django.views.generic import TemplateView
from django.shortcuts import render, redirect
from course.models import Course
from core.forms import EditProfileForm
from django.contrib.auth.models import User


class IndexView(TemplateView):
    template_name = 'core/index.html'


class ProfileView(TemplateView):
    template_name = 'core/profile.html'

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        context["courses"] = Course.objects.filter(
            purchased_by=self.request.user
        )
        return context


# class ProfileEditView(TemplateView):
#     template_name = 'core/profile_edit.html'
#
#     def post(self, request):
#         form = EditProfileForm(request.POST, instance=request.user)
#
#         if form.is_valid():
#             form.save()
#             return redirect('profile/')
#
#     def get(self, request):
#         form = EditProfileForm(instance=request.user)
#         return render(request, 'profile/', {'form': form})

def profile_edit(request):
    if request.method == "POST":
        form = EditProfileForm(request.POST, instance=request.user)

        if form.is_valid():
            form.save()
            return redirect("/profile/")
    else:
        form = EditProfileForm(instance=request.user)
        args = {'form':form}
        return render(request, 'core/profile_edit.html', args)


class StudentsView(TemplateView):
    template_name = 'core/students.html'

    def get_context_data(self, **kwargs):
        context = super(StudentsView, self).get_context_data(**kwargs)
        context["students"] = User.objects.filter(is_staff=False)
        context["courses_completed_by_students"] = []
        for student in context["students"]:
            completed_courses = Course.objects.filter(completed_by__username=student)
            if completed_courses:
                context["courses_completed_by_students"].append({
                    "name": student,
                    "courses": completed_courses
                })
        context["courses"] = Course.objects.filter(completed_by__username=self.request.user)
        return context


class Custom404View(TemplateView):
    template_name = '404/index.html'


class Custom403View(TemplateView):
    template_name = '403/index.html'


class Custom500View(TemplateView):
    template_name = '500/index.html'
