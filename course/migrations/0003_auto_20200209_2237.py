# Generated by Django 3.0.2 on 2020-02-09 22:37

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('course', '0002_course_purchased_by'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='course',
            name='purchased_by',
        ),
        migrations.AddField(
            model_name='course',
            name='purchased_by',
            field=models.ManyToManyField(blank=True, to=settings.AUTH_USER_MODEL),
        ),
    ]
