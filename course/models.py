from django.db import models
from django.core.validators import MinValueValidator
from django.contrib.auth.models import User


class Lesson(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField()
	video = models.URLField(max_length=250)
	homework = models.TextField(null=True)
	completed_by = models.ManyToManyField(User, blank=True, default=None)

	def __str__(self):
		return self.name


class Category(models.Model):
	name = models.CharField(max_length=50)

	def __str__(self):
		return self.name


class Course(models.Model):
	name = models.CharField(max_length=30)
	description = models.TextField()
	category = models.ForeignKey(Category, on_delete=models.PROTECT)
	cost = models.IntegerField(default=50, validators=[MinValueValidator(50)])
	lessons = models.ManyToManyField('Lesson', blank=True)
	purchased_by = models.ManyToManyField(User, blank=True, related_name='users')
	completed_by = models.ManyToManyField(User, blank=True, default=None)

	def __str__(self):
		return self.name


class Comment(models.Model):
	course = models.ForeignKey(Course, on_delete=models.CASCADE)
	autor = models.CharField(max_length=30)
	text = models.TextField()

	def __str__(self):
		return self.autor
