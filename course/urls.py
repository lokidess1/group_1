from django.urls import path, include

from .views import CourseViews, LessonViews, ThanksYouViews, CommentCreate

app_name = 'course'
urlpatterns = [
    path('<course_id>/', CourseViews.as_view(), name='course'),
    path('<course_id>/thanksyou/', ThanksYouViews.as_view(), name='thanksyou'),
    path('<course_id>/lesson_<lesson_id>', LessonViews.as_view(), name='lesson'),
    path('<course_id>/create-comment/', CommentCreate.as_view(), name='create-comment'),
]