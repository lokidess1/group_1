import stripe

from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.conf import settings
from django.shortcuts import render

from .models import Course, Lesson

stripe.api_key = settings.STRIPE_SECRET_KEY


class CourseViews(TemplateView):
	template_name = '403/index.html'

	def get(self, request, course_id, **kwargs):
		try:
			course = Course.objects.filter(id=course_id).select_related('category').prefetch_related('lessons', 'purchased_by')[0]
			comments = course.comment_set.all()

			if request.user in course.purchased_by.all():
				context = super().get_context_data(**kwargs)
				context['course'] = course
				context['comments'] = comments
				context["completed_courses"] = Course.objects.filter(completed_by__username=self.request.user)

				return render(request, 'course/course.html', context)

			else:
				context = super().get_context_data(**kwargs)
				context['key'] = settings.STRIPE_PUBLISHABLE_KEY
				context['course'] = course
				context['comments'] = comments

				return render(request, 'course/payment_page.html', context)
		
		except IndexError:
			return render(request, '404/index.html')


class LessonViews(TemplateView):
	template_name = '403/index.html'

	def get(self, request, course_id, lesson_id):
		try:
			course = Course.objects.filter(id=course_id).prefetch_related('lessons', 'purchased_by')[0]
			completed_lessons = Lesson.objects.filter(completed_by__username=self.request.user)

			if request.user in course.purchased_by.all():
				lesson = course.lessons.filter(id=lesson_id)[0]

				return render(request, 'course/lesson.html', {'course_id': course_id, 'lesson': lesson, 'completed_lessons': completed_lessons})

			else:
				return render(request, self.template_name)

		except IndexError:
			return render(request, '404/index.html')


class ThanksYouViews(TemplateView):
	template_name = '403/index.html'

	def post(self, request, course_id):

		course = Course.objects.filter(id=course_id).prefetch_related('purchased_by')[0]

		course.purchased_by.add(request.user)
		course.save()

		charge = stripe.Charge.create(
            amount=course.cost,
            currency='usd',
            description=f'Buy course with id = {course_id}',
            source=request.POST['stripeToken']
        )

		return render(request, 'course/thankyou.html')


class CommentCreate(TemplateView):
	template_name = '403/index.html'

	def post(self, request, course_id):
		course = Course.objects.filter(id=course_id).select_related('category').prefetch_related('lessons', 'purchased_by')[0]

		if request.POST['text'].replace(" ", ""):
			course.comment_set.create(autor=request.user.username, text=request.POST['text'])
			course.save()

		else:
			pass

		return HttpResponseRedirect(reverse('course:course', args=(course_id,)))
