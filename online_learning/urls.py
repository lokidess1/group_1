"""online_learning URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings

from core.views import IndexView, ProfileView, StudentsView, Custom404View, Custom403View, Custom500View
from register import views as v
from core import views as edit_view

handler404 = Custom404View.as_view()
handler403 = Custom403View.as_view()
#handler500 = Custom500View.as_view()

urlpatterns = [
    path('admin/', admin.site.urls, name='admin'),

    path('', IndexView.as_view(), name='home'),
    path('catalog/', include('catalog.urls'), name='catalog'),
    path('course/', include('course.urls')),

    # new user register
    path('register/', v.register, name='register'),
    # user login
    path('', include("django.contrib.auth.urls")),
    # social account login
    path('accounts/', include('allauth.urls')),

    # user profile and edit
    path('profile/', ProfileView.as_view()),
    path('profile/edit/', edit_view.profile_edit, name='profile_edit'),
    # path('profile/edit/', ProfileEditView.as_view())

    # all students page
    path('students/', StudentsView.as_view())
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),

        # For django versions before 2.0:
        # url(r'^__debug__/', include(debug_toolbar.urls)),

    ] + urlpatterns
